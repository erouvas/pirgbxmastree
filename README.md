# Raspberry Pi 4 RGB Christmas Lights

Festive lights are to be used with the [3D RGB Xmas Tree for Raspberry Pi](https://thepihut.com/products/3d-rgb-xmas-tree-for-raspberry-pi) from "The Pi Hut".

Exanding on the code from the [github repository](https://github.com/ThePiHut/rgbxmastree) `festive_lights.py` implements six paterns for the LEDs.

For a preview of the patterns click [here](https://dev.azure.com/rouvas/c883b948-11b8-4e60-8181-f2d9d6cad2f4/_apis/git/repositories/28675123-a5a3-41d0-b961-d38b7f3df1c1/items?path=%2Fpirgbxmas-preview.gif&versionDescriptor%5BversionOptions%5D=0&versionDescriptor%5BversionType%5D=0&versionDescriptor%5Bversion%5D=main&resolveLfs=true&%24format=octetStream&api-version=5.0)

## Usage

* Clone this repository with `https://gitlab.com/erouvas/pirgbxmastree.git`
* Change into the `pirgbxmastree` repository with `cd pirgbxmastree`
* Light up the Christmas tree with `python3 festive_lights.py`

### Tips and gotchas

* You might connect the "leafs" of the tree in the reverse order. In this case the LEDs will always remain on and white. Just power off your Pi and re-connect the "leafs" in reverse.
* While running the `festive_lights.py` execution can be stopped by pressing `Ctrl-C` and all lights in the tree should be turned off. If that is not the case, cycle power the Pi. Simply restarting will not do.
* Having the tree displaying patterns for an extended period of time, e.g. for some days, may result in the LEDs behaving erratically by either failing to change colours or not lighting at all. Should you encounter this, disconnect the Pi from the power source before powering up again.

Have fun and keep on tinkering !


