#
# - festive_lights.py - Dec 2020 
#   erouvas@yahoo.com
#

from tree import RGBXmasTree
from colorzero import Color
import random
import time
import queue
import random

tree = RGBXmasTree()

colors = [Color('red'), Color('green'), Color('blue')] # add more if you like

#ndx=0
#tree.color=(0,0,0)
#for pixel in tree:
#    ndx+=1
#    pixel.color=(1,0,0)
#    print('NDX=',ndx)
#    key = input('Press any key...')
#    pixel.color=(0,0,0)

try:
    while True:
        tree.brightness=1
        #
        # christmas lights merry go-round
        #
        tinsels=[2,4,6,9,12,15,18,21,24]
        tree.brightness=0.1
        tree.color=(0,1,0)
        for t in tinsels:
            tree[t-1].color=(1,0,0)
        time.sleep(1)
        q=queue.Queue(2)
        for j in [1,2]:
            if (j==2):
                tree.color=(0,0,0)
            for i in range(20):
                for t in tinsels:
                    tree[t-1].color=(1,0,0)
                    if (q.full()):
                        p=q.get()
                        tree[p-1].color=(0,0,0)
                    q.put(t)
        tree.color=(0,0,0)
        time.sleep(0.5)
        tree.brightness=1
        #
        # random around christmas lights
        #
        for t in tinsels:
            tree[t-1].color=(1,0,0)
        for i in range(90):
            p=random.randint(1,25)
            g=True
            for t in tinsels:
                if (t==p):
                    g=False
            if (g):
                tree[p-1].color=(0,1,0)
                time.sleep(0.1)
                tree[p-1].color=(0,0,0)
        tree.color=(0,0,0)
        time.sleep(0.5)
        #
        # christmas lights merry go-round - inverted
        #
        tinsels=[2,4,6,9,12,15,18,21,24]
        tree.brightness=0.1
        tree.color=(1,0,0)
        for t in tinsels:
            tree[t-1].color=(0,1,0)
        time.sleep(1)
        q=queue.Queue(2)
        for j in [1,2]:
            if (j==2):
                tree.color=(0,0,0)
            for i in range(20):
                for t in tinsels:
                    tree[t-1].color=(0,1,0)
                    if (q.full()):
                        p=q.get()
                        tree[p-1].color=(0,0,0)
                    q.put(t)
        tree.color=(0,0,0)
        time.sleep(0.5)
        tree.brightness=1
        #
        # 3-head - 3 flash
        #
        r = random.randrange(1,10)
        for color in colors:
            tree.color=(0,0,0)
            q = queue.Queue(3)
            time.sleep(r/10)
            for pixel in tree:
                pixel.color = color
                if (q.full()):
                    p=q.get()
                    p.color=(0,0,0)
                q.put(pixel)
                #pixel.color = (0,0,0)
            for i in [1,2,3]:
                tree.color=(0,0,0)
                time.sleep(0.2)
                tree.color=color
                time.sleep(0.2)
        tree.color=(0,0,0)
        time.sleep(0.5)
        #
        # 10-head snake - 3 colors loop 3 times
        #
        q = queue.Queue(10)
        for i in [1,2,3]:
            for color in colors:
                for pixel in tree:
                    pixel.color = color
                    if (q.full()):
                        p=q.get()
                        p.color=(0,0,0)
                    q.put(pixel)
        tree.color=(0,0,0)
        time.sleep(0.5)
        #
        # rotate spectrum
        #
        for r in range(0,255,40):
            for g in range(0,255,40):
                for b in range(0,255,40):
                    tree.color=(r,g,b)
        tree.color=(0,0,0)
        time.sleep(0.5)
        #
        # random led, random color, 3 open
        #
        q = queue.Queue(3)
        for i in range(120):
            if (q.full()):
                p=q.get()
                tree[p-1].color=(0,0,0)
            p=random.randint(1,25)
            tree[p-1].color=(random.randint(0,1),random.randint(0,1),random.randint(0,1))
            q.put(p)
        tree.color=(0,0,0)
        time.sleep(0.5)
        #
        # cycle brightness
        #
        for i in [1,2,3]:
            for color in colors:
                tree.color=color
                for b in range(100,0,-1):
                    tree.brightness = b/100
                    #time.sleep(0.1)
        tree.color=(0,0,0)
        time.sleep(0.5)
except KeyboardInterrupt:
    tree.color=(0,0,0)
    tree.close()
finally:
    tree.color=(0,0,0)
    tree.close()
    
print("END_RUN")
